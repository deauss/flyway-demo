/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : devopsone_core

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2017-10-24 14:53:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(255)     DEFAULT NULL
  COMMENT '姓名',
  `password`    VARCHAR(255)     DEFAULT NULL,
  `mobile`      VARCHAR(255)     DEFAULT NULL
  COMMENT '手机号',
  `email`       VARCHAR(255)     DEFAULT NULL
  COMMENT '电子邮箱',
  `create_user` INT(11)          DEFAULT NULL
  COMMENT '创建人',
  `cteate_time` DATETIME         DEFAULT NULL
  COMMENT '创建时间',
  `update_user` INT(11)          DEFAULT NULL
  COMMENT '更新人',
  `update_time` DATETIME         DEFAULT NULL
  COMMENT '更新时间',
  `remark`      VARCHAR(255)     DEFAULT NULL
  COMMENT '备注',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;